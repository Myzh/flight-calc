CP="$(pwd)/../lib/*"

for file in $(find /usr/local/bin/java -name '*.jar'); do
    CP="$CP:$file"
done;

export CLASSPATH=$CP
javac flightcalc/ui/MainPanel.java

CP="$CP:$(pwd)"
OPTIONS="--module-path /usr/local/bin/java/javafx --add-modules=javafx.controls -cp $CP"

java $OPTIONS flightcalc.ui.MainPanel
