package flightcalc.ui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class MainPanel extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane root = new Pane();
        Scene scene = new Scene(root, 300, 200);

        GridPane layout = new GridPane();

        layout.add(new Label("Flight level calculator"), 0, 0);
        Button but1 = new Button("Open");
        but1.setOnAction(e -> {System.out.println("Flight level calculator");});
        layout.add(but1, 1, 0);

        layout.add(new Label("Radar range calculator"), 0, 1);
        Button but2 = new Button("Open");
        but2.setOnAction(e -> {System.out.println("Radar range calculator");});
        layout.add(but2, 1, 1);

        layout.add(new Label("Radar resolution calculator"), 0, 2);
        Button but3 = new Button("Open");
        but3.setOnAction(e -> {System.out.println("Radar resolution calculator");});
        layout.add(but3, 1, 2);

        layout.setStyle("-fx-padding: 20px; -fx-vgap: 20px; -fx-hgap: 20px");

        root.getChildren().add(layout);

        // link with primaryStage
        primaryStage.setScene(scene);
        primaryStage.setTitle("Flight Calculator");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}